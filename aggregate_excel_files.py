import pandas as pd
import os

# Create dataframe
df_final = pd.DataFrame()

# Crawl subfolders
for root, dirs, files in os.walk("."):
    # Skip hidden folders
    files = [f for f in files if not f[0] == '.']
    dirs[:] = [d for d in dirs if not d[0] == '.']

    # Get sample and species name
    sample = os.path.split(root)[-1]
    species = [f for f in files if f.endswith(".csv")]

    # Read csvs and add additional columns
    for spec in species:
        df_temp = pd.read_csv(os.path.join(root, spec))
        df_temp["species"], _ = os.path.splitext(spec)
        df_temp["sample"] = sample

        # Concatenate dataframes
        df_final = pd.concat([df_final, df_temp])

# Save
df_final.to_csv("lahnewiesgraben.csv")
